# MongoDB Cheatsheet

## Notas

Cuando se pregunta por `null` puede ser porque:

- El campo es `null`.
- El campo no existe.

## Glosario equivalencias con SQL

MongoDB 					| SQL
------------------------ | ------------------------
Documento 				| Registro
Colección				| Tablas



## Crear una BD

### Creamos/entramos en la base de datos a crear

```javascript
use juegoMongodb;
```

### Creamos el documento a insertar en la nueva BD

```javascript	
var juego = {
    "titulo": "Witcher 3",
    "descripcion": "Juego RPG creado por CDProject",
    "fecha_lanzamiento": new Date("2013-05-12")
};
```

### Insertamos el documento creado en la colección

```javascript
db.juegos.insert(juego);
```

- `db`: representa la base de datos en uso.
- `juegos`: colección a crear (si la colección no existe, se crea).
- `.insert(juego)`: operación. Insertar en la colección `juegos` el documento `juego`.

### Comprobamos que la colección se haya creado

```javascript
show dbs;
```


## Insertar JSON en mongo

```bash
mongo < insertarPeliculas.js
```

### Ejemplo fichero JS para insertar datos

```javascript
var peliculas = [
    {
            "title" : "La lengua de las mariposas",
            "contentType" : "pelicula",
            "description" : "Situada en 1936, Don Gregorio enseñará a Moncho con dedicación y paciencia toda su sabiduría en cuanto a los conocimientos, la literatura, la naturaleza, y hasta las mujeres.",
            "ratings" : [
                {
                    "nick" : "Fernando",
                    "rating" : 9,
                    "commentary" : "Magnífica."
                }
            ],
            "genre" : ["Drama"],
            "viewsNumber" : 8134,
            "awards" : ["1999: Premios Goya: Mejor guión adaptado. 13 nominaciones.", "1999: Premios Forqué: Nominada a Mejor película"],
            "duration" : 97,
            "director" : ["José Luis Cuerda"]
    },
    {
            "title" : "La soledad del corredor de fondo",
            "contentType" : "pelicula",
            "description" : "Colin Smith es un joven de clase obrera que vive en los alrededores de Nottingham.",
            "ratings" : [
                {
                    "nick" : "Mikel",
                    "rating" : 8,
                    "commentary" : "Muy buena"
                }
            ],
            "genre" : ["Drama"],
            "viewsNumber" : 2000,
            "awards" : ["1962: Premios BAFTA: Mejor promesa (Tom Courtenay)"],
            "duration" : 99,
            "director" : ["Tony Richardson"]
    }, 
];

var db = connect("localhost:27017/admin");
db.auth('proyecto', 'Mongodb123');
db = db.getSiblingDB('NetMongox');

// Variable `peliculas` definida arriba
db.contenidos.insert(peliculas);
```

## Comandos útiles

### Mostrar BD en uso

```javascript
 db;
```

### Mostrar todas las BD

```javascript
show dbs;
```

### Entrar/usar en una BD

```javascript
use nombreBaseDatos;
```

### Obtener el primer documento de la colección `juegos`

```javascript
db.juegos.findOne();
```

### Obtener todos los documentos de la colección `juegos`

```javascript
db.juegos.find().pretty();
```


## Actualizar documento

### Modificamos alguna propiedad de una variable/documento almacenada 

- Cambiamos el valor de `descripcion` del documento existente `juego`.

```javascript
juego.descripcion = "Cambiado texto";
```

### Actualizamos/aplicamos

```javascript
db.juegos.update({"titulo": "Witcher 3"}, juego);
```
Argumentos:

- 1º argumento: Consulta (WHERE en SQL) los documentos donde se aplicaran los cambios.
- 2º argumento: JSON o variable/documento con los que actualizar el documento coincidente.


### Comprobamos que se haya actualizado

```javascript
db.juegos.findOne();
```


## Métodos

### remove()

> Elimina documentos de una colección.

Parámetros: 

1. Consulta
2. (opcional): boolean. Elimina un documento o todos los que coincidan. (Si se omite, elimina todos).

```javascript
db.auriculares.remove({"marca": "sony"}, true);
db.auriculares.remove({potencia: 8});
```

### update()

> Actualiza uno o varios documentos de una colección.

Parámetros: 

1. Consulta: Criterio de selección; documentos a actualizar.
2. Actualización: Los nuevos documentos.
3. `upsert`: 
   - `true`: Crea un nuevo documentos cuando no existe.
   - `false`: No crea un nuevo documentos cuando no existe.
4. `multi`: 
   - `true`: Actualiza todos los documentos que coincidan.
   - `false`: Solo actualiza el primer documentos que coincide.
   

```javascript
db.juegos.update( 
   {genero: "plataformas"}, 
   {
      titulo: "PlataformasEjemplo", 
      descripcion: "ejemplo de plataformas", 
      genero: "plataformas"
   }, 
   true,
   true
);
```

```javascript
db.juegos.update(
   {genero: "RPG"}, 
   {
      $set: {valoracion: 7}
   }, 
   true, 
   true
);
```

### save()

> Actualiza o inserta un nuevo documento.

**Nota:** Si **no** cambiamos el `_id` el documento se actualiza/sobrescribe.

#### Uso
##### Guardamos el documento de un `find() / findOne()` en una variable

```javascript
var copiaJuego = db.juegos.findOne();
```

##### Modificamos algunos valores

**Importante**: Modificamos el `_id` a uno que **no exista** para hacer una copia y evitar que se sobrescriba.

```javascript
copiaJuego._id = 3; 
copiaJuego.titulo = "Clon Witcher 3";
copiaJuego.valoracion = 99;
```

##### Finalmente lo aplicamos

```javascript
db.juegos.save(copiaJuego);
```


### find()

> Selecciona los documentos de una colección.  

Parámetros: 

1. Consulta (opcional): selecciona todos los documentos que coincidan con los campos. Se aplica un `AND` de SQL.
2. `projection` (opcional): se especifican los campos a seleccionar utilizando `true` / `false` o simplemente
   omitiéndolos.

```javascript
db.juegos.find(
   {"genero": "RPG", valoracion: 7}, 
   {titulo: true, descripcion: true, genero: true, plataformas: true, _id: false}
).pretty()
```

#### Muestra todos los juegos que en el array `plataformas[]` tienen "PC" como campo.

```javascript
db.juegos.find({plataformas: "PC"}).pretty()
```

## Operadores

Leyenda:

- Principio básico de los operadores: si el campo especificado no existe, el campo se crea automáticamente.
- w = with
- durante = después de 

### Operadores de comparación

- `$eq`: Igual (==)
- `$gt`: Mayor (>)
- `$gte`: Mayor o igual (>=)
- `$in`: Cualquier valor especificado en un array
- `$lt`: Menor (<)
- `$lte`: Menor o igual (<=)
- `$ne`:  No igual / diferente (!=)
- `$nin`: Ninguno de los valores especificados en un array

#### $gte,  $lt
##### Mostrar los documentos que tengan 	`numVentas >= 8 y/AND numVentas < 12`

```javascript
db.juegos.find({numVentas: {$gte:8, $lt:12}}).pretty();
```

#### $not, $gte,  $lt
#### Mostrar los documentos que no coinciden

```javascript
db.juegos.find({numVentas: {$not: {$gte:8, $lt:12}} }).pretty():
```

#### $in 

> Selecciona los documentos donde el valor del campo es igual a cualquiera/alguno/uno de los valores del array.

**Nota:** Similar al operador lógico `OR`.

#### Muestra solo los documentos en los cuales `numVentas` sea `8`, `9` o `10`.

```javascript
db.juegos.find({numVentas: {$in: [8, 9, 10]} }).pretty();
```


### Operadores de arrays

#### $all

> Selecciona los documentos donde coincida todos los elementos del array con el campo especificado.

```javascript
db.juegos.find({plataformas:{$all: ["PC", "XBOX"]}}).pretty()
```

##### Equivalencia, sin operadores

```javascrit
db.juegos.find({plataformas: "PC", plataformas: "XBOX"}).pretty()
```


#### $size

> Selecciona cualquier campo de tipo array que tenga exactamente el número de elementos especificado. Ni uno más, ni uno menos.

##### Muestra los documentos que en el array `plataformas[]` contengan exactamente 2 elementos.

```javascript
db.juegos.find({plataformas:{$size:2}}).pretty();
```


### Operadores de evaluación

#### $where

> Pasa una expresión JavaScript al sistema de consultas.

**Notas:**

- El rendimiento de este operador es bajo ya que requiere que la BD procese JS. Por lo tanto, solo se recomienda utilizar como última opción. 
- `this` representa el documento actual del cursor (`find()` crea un cursor)

Dependiendo del valor de retorno:

   - `true`: Aparece en la consulta.
   - `false`: No aparece en la consulta.

```javascript
db.juegos.find({
	$where: function() {
      if (this.comentarios.length > 1) {
         return this.comentarios[0].length > this.comentarios[1].length;
      } else {
         return true;
      }
   }
});
```



### Operadores lógicos

#### $not

> Selecciona los documentos que no coinciden con la consulta.

- Es un comparador lógico.

```javascript
db.juegos.find({numVentas: {$not:{$gte:8, $lt:12).pretty()
```

#### $or

> Selecciona el/los documento/s que coinciden al menos con una de las expresiones.

**Nota:** Equivalente al operador lógico `OR` de SQL.

```javascript
db.juegos.find({$or:[{numVentas:{$lte:8}}, {precio:{$gte:12}}]}).pretty();
```


### Otros operadores
#### $set

> Reemplaza el valor del campo

```javascript
db.juegos.update( 
   {"_id" : ObjectId("5e68e6494e4eedbeb029cb16")},
   {
      $set: {numVentas: 10}
   }
);
```

```javascript
db.juegos.update( 
   {"_id" : ObjectId("5e68e6494e4eedbeb029cb16")}, 
   { 
      $set: { 
         plataformas: ["XBOX", "PC"], comentarios: ["Me ha gustado mucho",]
      } 
   } 
);
```

##### Actualizar el valor `puntuacion` en la 2ª posición del array `valoraciones`

***Importante:*** Reparar en las comillas "" del `$set` para especificar que es un array.

```javascript
db.auriculares.update( 
	{"_id" : ObjectId("5e818f9c1255d8d329043928")}, 
	{
		$set: {"valoraciones.1.puntuacion": 6}
	} 
)
```


##### Crear un campo de tipo array

###### Utilizar `[]` para especificar que es un array.

```javascript
db.juegos.update( 
   {"_id" : ObjectId("5e68e6494e4eedbeb029cb16")}, 
   {
      $set: {comentarios: ["me ha gustado mucho"] }
   }
);
```


#### $unset

> Elimina el campo especificado.

**Nota:** Elimina el campo y lo establece en `null`. Para eliminarlo definitivamente utilizar `$pull`.

```javascript
db.juegos.update( 
   {"_id" : ObjectId("5e68e6494e4eedbeb029cb16")}, 
   {
      $unset: {plataformas:""}
   }
);
```

##### Eliminar los campos que coincidan con la sentencia

```javascript
db.juegos.update(
   {comentarios: "comentario2"},
   {
      $unset: {"comentarios.$": 1}
   }
);
```

**Nota:** `$` es el numero (ID) del campo que coincide. El `1` no es nada.

##### Eliminar el tercer valor del array (.3)

```javascript
db.juegos.update( 
   {"_id" : ObjectId("5e68e622c91cd589002cb526")},
   { 
      $unset: { 
         "comentarios.3":  1 
      } 
   } 
);
```


#### $inc

> Incrementa el valor del campo

```javascript
db.juegos.update( 
   {"_id" : ObjectId("5e68e6494e4eedbeb029cb16")}, 
   {
      $inc: {numVentas: 5}
   } 
);
```

#### $push

> Añade el valor especificado a un array

```javascript
db.juegos.update( 
   {"_id" : ObjectId("5e68e6494e4eedbeb029cb16")}, 
   {
      $push: {comentarios: "Es estupendo"}
   }
);
```

#### $each (w: $push)

> Añade múltiples valores a un array

##### Creamos un array. Después lo añadimos a un campo array con `$push`

```javascript
var varComentarios = ["Genial", "Soberbio", "Maravilloso" ];

db.juegos.update( 
   {"_id" : ObjectId("5e68e6494e4eedbeb029cb16")}, 
   {
      $push: {comentarios: {$each: varComentarios}
   }
);
```

#### $slice (w: $push, $each)

> Limita el numero de elementos durante un $push

Valores:

- `0`: vacía el array
- `num negativo`: los últimos `num` elementos
- `num positivo`: los primeros `num` elementos

```javascript
db.juegos.update( 
   {"_id" : ObjectId("5e68e6494e4eedbeb029cb16")}, 
   {
      $push: {
         comentarios: {
            $each: comentariosVar, 
            $slice: -1
         }
      }
   }
);
```

#### $sort (w: $push, $each)

> Ordena los elementos durante un `$push`

Valores de `$sort`:

- `1`: Ascendente 
- `-1`: Descendente

Usos `$each`:

- `$each: []`: No añade nada. Solo ordena.
- `$each: nombreVariable`: Añade los valores de la variable y los ordena, porque estamos haciendo un `$push`

```javascript
db.juegos.update( 
   {"_id" : ObjectId("5e68e622c91cd589002cb526")}, 
   { 
      $push: { 
         comentarios: { 
            $each: [], 
            $sort: 1 
         }
      } 
   } 
);
```

#### $pull

> Elimina de un array todos los elementos que coincidan con la condición/valor.

```javascript
db.juegos.update(
   {"_id" : ObjectId("5e68e622c91cd589002cb526")},
   {
      $pull:{comentarios: "Maravilloso"}
   }
);
```

##### Eliminar todos los campos con `valor: null`

```javascript
db.juegos.update( 
   {"_id" : ObjectId("5e68e622c91cd589002cb526")}, 
   { 
      $pull: { "comentarios":  null} 
   }
);
```

#### $pop

> Elimina el primero o el último elemento de un array.

Valores: 

-  `-1`: Elimina el primer elemento
- `1`: Elimina el último elemento

```javascript
db.juegos.update( 
   {"_id" : ObjectId("5e68e622c91cd589002cb526")}, 
   { 
      $pop: { "comentarios": -1}
   }
); 
```

## Métodos

### sort({})

> Especifica el orden en el cual la consulta (`find()`) se debe mostrar.

Parámetro: 

- Campos a ordenar y su orden: `{numVentas: 1, precio: -1}`

Orden:

- `1`: Ascendente
- `-1`: Descencente

```javascript
db.juegos.find().sort({numVentas:-1}).pretty()
```


#### Se ordenan por `numVentas`. En caso de que `numVentas` haya alguna coincidencia, se ordena por el criterio de `titulo`.

```javascript
db.juegos.find().sort({numVentas:-1, titulo: 1}).pretty()
```


### skip()

> Salta el número especificado de documentos del resultado.

#### Salta los primeros 4 documentos. Muestra a partir de 5º, el 5º incluido.

```javascript
db.juegos.find().skip(4).pretty()
```


### limit() 

> Especifica el número máximo de documentos a retornar. Como un `LIMIT` de SQL.

**Nota:** Reparar en la diferencia con `$slice`

```javascript
db.juegos.find().limit(1).pretty()
```



